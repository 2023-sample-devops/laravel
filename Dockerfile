# Use PHP-FPM base image
FROM php:8.3-rc-fpm-alpine

# Set up working directory
WORKDIR /var/www/html

# Install additional packages and open the PHP utilities you need
RUN apk update && apk add libpng libpng-dev libjpeg-turbo libjpeg-turbo-dev freetype freetype-dev libwebp libwebp-dev zlib-dev zip unzip git && \
    docker-php-ext-configure gd --with-freetype --with-jpeg --with-webp && \
    docker-php-ext-install gd pdo pdo_mysql

# Copy the Laravel application source code into the Docker image
COPY . .

# Install Composer and dependencies
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer && \
    composer install && \
    chown -R www-data:www-data storage bootstrap/cache

# Launch the Laravel application
CMD php artisan serve --host=0.0.0.0
