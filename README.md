# How to use this application?

## Table of contents

- [1. Use in local environment](#1-use-in-local-environment)
- [2. Automatically build and push to ECR with Gitlab CI](#2-automatically-build-and-push-to-ecr-with-gitlab-ci)

## 1. Use in local environment

I assume that your computer already has docker installed.I have written docker-compose for the local environment, you run the following command:

```sh
docker-compose up -d --build
```

Your terminal window will look like this.

![docker-compose](./readme_images/docker-compose.jpg)

You can then visit http://localhost:8000 to see the laravel application in action.

![localhost](./readme_images/localhost.jpg)

After using it, run the command below to delete the current docker compose.

```sh
docker-compose down
```

## 2. Automatically build and push to ECR with Gitlab CI

The laravel application has been set up to automatically build and push images to ECR.

First, you need to create an ECR repository. You can use this [Terraform repository](https://gitlab.com/2023-sample-devops/terraform) to do that.

Afterward, you need to declare some variables to use for CI the image below.

![gitlab_variables](./readme_images/gitlab_variables.jpg)

The pipeline's processing flow will be as follows:
- The default pipeline will run tests with built-in templates from Gitlab for all commits, including:
    - SAST
    - Secret Detection

- Next, with PRs, commits and mains and tags with prefix `app.2023`, the `build_and_scan` step will be performed. This step will build the docker image but not push it to ECR. It only scans the docker image with Trivy to ensure that the image has no vulnerabilities before pushing it to ECR.

- The last step is `build_and_push`, this is the step after the `build_and_scan` step is successful, it will be performed. The condition only applies to commits to main and tag. This step will build the image, scan the image with trivy again and push it to ECR.

![pipeline](./readme_images/pipeline.jpg)
![build_and_scan](./readme_images/build_and_scan.jpg)
![build_and_push](./readme_images/build_and_push.jpg)

During the pipeline process of building the docker image and pushing it to ECR, if the testing steps fail or Trivy discovers a security vulnerability, the pipeline will be returned to a failed status.

And here are the results on ECR.

![ecr](./readme_images/ecr.jpg)

After uploading the image to ECR, we can pull it back and run it on any computer. Or deploy into K8S clusters.
